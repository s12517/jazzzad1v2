package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class index_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.Vector _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html; charset=ISO-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\r\n");
      out.write("<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\r\n");
      out.write("<html>\r\n");
      out.write("<head>\r\n");
      out.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=ISO-8859-1\">\r\n");
      out.write("<title>Zadanie zaliczeniowe nr 1</title>\r\n");
      out.write("</head>\r\n");
      out.write("<body>\r\n");
      out.write("<form action=\"schedule\" method=\"post\">\r\n");
      out.write("<label>Wnioskowana kwota Kredytu:<input type=\"text\" id=\"loan\" name=\"loan\"/></label><br>\r\n");
      out.write("<label>Ilosc rat:<input type=\"text\" id=\"installmentAmount\" name=\"installmentAmount\"/></label><br>\r\n");
      out.write("<label>Oprocentowanie:<input type=\"text\" id=\"bankrate\" name=\"bankrate\"/></label><br>\r\n");
      out.write("<label>Oplata stala:<input type=\"text\" id=\"fixedprice\" name=\"fixedprice\"/></label><br>\r\n");
      out.write("<label>Rodzaj rat:<br>\r\n");
      out.write("malejaca<input type=\"radio\" id=\"type\" name=\"type\" value=\"decreasing\"/>\r\n");
      out.write("stala\r\n");
      out.write("<input type=\"radio\" id=\"type\" name=\"type\" value =\"static\"/></label><br>\r\n");
      out.write("\r\n");
      out.write("<input type = \"submit\" value=\"wyslij\"/>\r\n");
      out.write("</form>\r\n");
      out.write("</body>\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
