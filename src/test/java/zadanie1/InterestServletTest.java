package zadanie1;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Test;
import org.mockito.Mockito;

public class InterestServletTest extends Mockito {

	@Test
	public void servlet_should_not_accept_loan_equal_to_null() throws IOException {
		HttpServletRequest request = mock(HttpServletRequest.class);
		HttpServletResponse response = mock(HttpServletResponse.class);
		PrintWriter writer = mock(PrintWriter.class);
		when(response.getWriter()).thenReturn(writer);
		InterestServlet servlet = new InterestServlet();
		
		when(request.getParameter("loan")).thenReturn("0");
		when(request.getParameter("installmentAmount")).thenReturn("1000");
		when(request.getParameter("bankrate")).thenReturn("4");
		when(request.getParameter("fixedprice")).thenReturn("5");
		servlet.doPost(request, response);
		verify(response).sendRedirect("/");
	}
	
	@Test
	public void servlet_should_not_accept_installmentAmount_equal_to_null() throws IOException {
		HttpServletRequest request = mock(HttpServletRequest.class);
		HttpServletResponse response = mock(HttpServletResponse.class);
		PrintWriter writer = mock(PrintWriter.class);
		when(response.getWriter()).thenReturn(writer);
		InterestServlet servlet = new InterestServlet();
		
		when(request.getParameter("loan")).thenReturn("100000");
		when(request.getParameter("installmentAmount")).thenReturn("0");
		when(request.getParameter("bankrate")).thenReturn("4");
		when(request.getParameter("fixedprice")).thenReturn("5");
		servlet.doPost(request, response);
		verify(response).sendRedirect("/");
	}
	
	@Test
	public void servlet_should_not_accept_bankRate_equal_to_null() throws IOException {
		HttpServletRequest request = mock(HttpServletRequest.class);
		HttpServletResponse response = mock(HttpServletResponse.class);
		PrintWriter writer = mock(PrintWriter.class);
		when(response.getWriter()).thenReturn(writer);
		InterestServlet servlet = new InterestServlet();
		
		when(request.getParameter("loan")).thenReturn("1000000");
		when(request.getParameter("installmentAmount")).thenReturn("1000");
		when(request.getParameter("bankrate")).thenReturn("0");
		when(request.getParameter("fixedprice")).thenReturn("5");
		servlet.doPost(request, response);
		verify(response).sendRedirect("/");
	}
	
	@Test
	public void servlet_should_not_accept_annualprice_equal_to_null() throws IOException {
		HttpServletRequest request = mock(HttpServletRequest.class);
		HttpServletResponse response = mock(HttpServletResponse.class);
		PrintWriter writer = mock(PrintWriter.class);
		when(response.getWriter()).thenReturn(writer);
		InterestServlet servlet = new InterestServlet();
		
		when(request.getParameter("loan")).thenReturn("10000000");
		when(request.getParameter("installmentAmount")).thenReturn("1000");
		when(request.getParameter("bankrate")).thenReturn("4");
		when(request.getParameter("fixedprice")).thenReturn("0");
		servlet.doPost(request, response);
		verify(response).sendRedirect("/");
	}
	
}
