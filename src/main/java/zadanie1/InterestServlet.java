package zadanie1;

import java.io.IOException;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import domain.Credit;

@WebServlet("/schedule")

public class InterestServlet extends HttpServlet {
	
	private Credit retriveApplicationFromRequest(HttpServletRequest request){
		Credit credit = new Credit();
		int loan =Integer.parseInt(request.getParameter("loan")) ;
		int installmentAmount=Integer.parseInt(request.getParameter("installmentAmount"));
		float bankrate =  Float.parseFloat(request.getParameter("bankrate"));
		float fixedprice = Float.parseFloat(request.getParameter("fixedprice"));
		
		if(loan<=0 || installmentAmount<=0 || bankrate<=0 ||fixedprice<=0){
			credit.setGoOn(false);		
		
		}else{
		credit.setLoan(loan);
		credit.setInstallmentAmount(installmentAmount);
		credit.setBankrate(bankrate);
		credit.setFixedprice(fixedprice);
		credit.setGoOn(true);
		credit.setType(request.getParameter("type"));
				
	}
		return credit;
	}
	private static final long serialVersionUID = 1L;
	
		
public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException{
	
	 Credit finalCredit = this.retriveApplicationFromRequest(request);
	 boolean goOn=finalCredit.getGoOn();
	 if(goOn==false){
	 response.sendRedirect("/");
	 }else{
	 ICanCalculateInstallments decrease = new DecreasingInstallment();
	 ICanCalculateInstallments annual = new AnnualInstallment();
	 float installments[][] = null;
	 if("static".equals(finalCredit.getType())){
		 installments = annual.installment(finalCredit); 
	 }else{
		 installments = decrease.installment(finalCredit);  
	 }
	
	response.setContentType("text/html");
	response.getWriter().println("<h1>Twoj harmonogram:</h1>");
	response.getWriter().println("<table = border = '1'><tr><td>Numer raty</td><td>Kwota kapitalu</td><td>Kwota odsetek</td><td>Oplaty stale</td><td>Calkowita kwota raty</td></tr>");
	for(int i = 0;i<=finalCredit.getInstallmentAmount()-1;i++){
		response.getWriter().println("<tr><td>"+installments[i][0]+"</td>	<td>" +installments[i][1]+"</td><td>" +installments[i][2]+"</td><td>" +installments[i][3]+"</td><td>" +installments[i][4]+"</td></tr>"); 
	}

	response.getWriter().println("</table>");
}
}
}
