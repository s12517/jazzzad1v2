package zadanie1;


import domain.Credit;

public class DecreasingInstallment implements ICanCalculateInstallments{

	@Override
	public float[][] installment(Credit fullcredit){
		
		
		float loan = fullcredit.getLoan();
		float bankrate=fullcredit.getBankrate();
		int installmentAmount = fullcredit.getInstallmentAmount();
		float fixedprice = fullcredit.getFixedprice() ;

		float interest = loan*((bankrate/100)/12);
		float capital = loan/installmentAmount;
		float wholeInstallment = interest + capital+fixedprice;
		
		float [][]InstArray = new float[installmentAmount][5];
		
		for(int i = 1;i<=installmentAmount;i++){
			loan -= capital;
		   interest = loan*((bankrate/100)/12);
		   wholeInstallment = interest + capital+fixedprice;
		  InstArray[i-1][0]= i;
		  InstArray[i-1][1]=  (float) (Math.floor(capital*100)/100);
		  InstArray[i-1][2]= (float) (Math.floor(interest*100)/100);
		  InstArray[i-1][3]= (float) (Math.floor(fixedprice*100)/100);
		  InstArray[i-1][4]= (float) (Math.floor(wholeInstallment*100)/100);
		  
}
		return (InstArray);
	}
}