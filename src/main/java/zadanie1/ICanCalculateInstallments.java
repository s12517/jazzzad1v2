package zadanie1;

import domain.Credit;

public interface ICanCalculateInstallments {

	float[][] installment(Credit credit);

}
