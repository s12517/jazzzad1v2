package zadanie1;

import domain.Credit;

public class AnnualInstallment implements ICanCalculateInstallments{
		@Override
		public float[][] installment(Credit fullcredit){
			
			
			float loan = fullcredit.getLoan();
			float bankrate=fullcredit.getBankrate();
			int installmentAmount = fullcredit.getInstallmentAmount();
			float fixedprice = fullcredit.getFixedprice() ;
			
			double q =  1+((bankrate/100)/12);
			float potegaq =  (float) Math.pow(q, installmentAmount);
			float installment = (float) (loan*potegaq*(q-1)/(potegaq-1));	
			float interest = (float) (loan* (bankrate/100)* (30.0/360.0));
			float capital = installment - interest;
			float wholeInstallment = interest + capital+fixedprice;
			
			
			float [][]InstArray = new float[installmentAmount][5];
			
			for(int i = 1;i<=installmentAmount;i++){
				loan -=capital;
			 interest = (float) (loan* (bankrate/100)* (30.0/360.0));
			 capital=installment-interest;
			  wholeInstallment = interest + capital+fixedprice;
			  InstArray[i-1][0]= i;
			  InstArray[i-1][1]=  (float) (Math.floor(capital*100)/100);
			  InstArray[i-1][2]= (float) (Math.floor(interest*100)/100);
			  InstArray[i-1][3]= (float) (Math.floor(fixedprice*100)/100);
			  InstArray[i-1][4]= (float) (Math.floor(wholeInstallment*100)/100);
			  
			
		}
			return (InstArray)  ;
	}

		}

