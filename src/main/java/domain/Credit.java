package domain;

public class Credit {
	
	private float loan;
	private int installmentAmount;
	private float bankrate;
	private float fixedprice;
	private String type;
	private boolean goOn;
	
	public Credit(float loan, int installmentAmount, float bankrate, float fixedprice, String type){
	
		this.loan = loan;
		this.installmentAmount = installmentAmount;
		this.bankrate = bankrate;
		this.fixedprice = fixedprice;
		this.type = type;
	}
	

	public Credit() {
		}


	public float getLoan() {
		return loan;
	}
	public void setLoan(float loan) {
		this.loan = loan;
	}
	public int getInstallmentAmount() {
		return installmentAmount;
	}
	public void setInstallmentAmount(int installmentAmount) {
		this.installmentAmount = installmentAmount;
	}
	public float getBankrate() {
		return bankrate;
	}
	public void setBankrate(float bankrate) {
		this.bankrate = bankrate;
	}
	public float getFixedprice() {
		return fixedprice;
	}
	public void setFixedprice(float fixedprice) {
		this.fixedprice = fixedprice;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}

	public boolean getGoOn() {
		return goOn;
	}

	public void setGoOn(boolean goOn) {
		this.goOn = goOn;
	}
	
	
	}
	

